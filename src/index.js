import angular from 'angular';

import uiRouter from 'angular-ui-router';
import uiFilter from 'angular-filter';
import components from './app/components/app.components';
import appContainer from './app/app.container';
import commons from './app/common/app.commons';
import core from './app/core/app.core';

import authInterceptor from './app/core/auth.interceptor';

import './index.scss';

const app = angular
  .module('app', [uiRouter, 'ui.bootstrap', 'angular.filter', core, commons, components ])
  .component('app', appContainer)
  .factory('authInterceptor', authInterceptor)
  .config(($stateProvider, $urlRouterProvider, $httpProvider) => {
    $httpProvider.interceptors.push('authInterceptor');
    $httpProvider.defaults.useXDomain = true;


    $stateProvider
      .state('app', {
        url: '/'
      });

    $urlRouterProvider.otherwise('/');
  });

export default app;
