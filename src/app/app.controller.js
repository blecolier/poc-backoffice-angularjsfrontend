class AppController{

  constructor(AppService, $state){
    'ngInject';

    this.appService = AppService;
    this.environment = this.appService.getAppEnvironment();
    this.title = this.appService.getAppTitle();
  }

  reloadApplicationTitle(){
    this.title = this.appService.getAppTitle();
  }

  userLogedIn(){
    return this.appService.logedIn;
  }
}

export default AppController;
