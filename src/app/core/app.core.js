import angular from 'angular';
import appService from './app.service';
import authService from './auth.service';
import authCurrentService from './auth.current.service';
//import authInterceptor from './auth.interceptor';
import restAuthService from './app.rest.service';

const core = angular
  .module('app.core', [])
  .service('AppService', appService)
  .service('AuthService', authService)
  .service('AuthCurrentService', authCurrentService)
  .service('RestAuthService', restAuthService)
  .name;

export default core;
