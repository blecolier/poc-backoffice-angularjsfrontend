
class BaseComponentController{
  constructor(appService, RestAuthService, pageName, moduleName) {
    this.appService = appService;
    this.restAuthService = RestAuthService;
    this.pageTittle = pageName;
    this.messages = [];
    this.moduleName = moduleName;
  }

  $onInit() {
    this.appService.reloadAppTitle(this.pageTittle);
  };

  canAction(action) {
      if (typeof this.moduleName === 'undefined' || typeof this.appService.currentUser === 'undefined') {
          return;
      }
      const _self = this;
      let module = this.appService.currentUser.modules.filter(function (item) {
          if (item.moduleName === _self.moduleName) {
              return item;
          }
      });
      return module[0] !== null && typeof module[0] !== 'undefined' && module[0].permission !== null
          && typeof module[0].permission !== 'undefined' ? module[0].permission.indexOf(action) > -1 : false;
  }

  addMessage(type, title, description){
    let index = this.messages.length;
    this.messages[index] = { type: type, title : title, description: description, index: index};
  }

  error(title, description){
    this.addMessage('error', title, description);
  }

  warning(title, description){
    this.addMessage('warning', title, description);
  }

  success(title, description){
    this.addMessage('success', title, description);
  }
}

export default BaseComponentController;
