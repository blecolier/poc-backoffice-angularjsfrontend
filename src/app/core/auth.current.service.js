
class AuthCurrentService{
  constructor($window){
    'ngInject';

    this._$window = $window;
  }

  getAuthenticationHeader(){
    let currentToken = this._$window.localStorage.jwtToken;
    return currentToken != null && currentToken != '' ? 'JWT ' + currentToken : null;
  }

  extractUserFromToken(){
    let user = new Buffer(this._$window.localStorage.jwtToken.split('.')[1], 'base64').toString();
    return JSON.parse(user.toString());
  }

  setCurrentToken(token){
    this._$window.localStorage.jwtToken = token;
      this._$window.localStorage.jwtTokenExpirationDate = token === '' ? 0 : this.extractUserFromToken().exp;
  }

  destroyToken(forceReload){
    this._$window.localStorage.removeItem('jwtToken');
    this._$window.localStorage.removeItem('jwtTokenExpirationDate');

    this._$window.location = '/';
  }

  getCurrentToken(){
    return this._$window.localStorage.jwtToken;
  }

  getTokenExpirationDate(){
    return this._$window.localStorage.jwtTokenExpirationDate;
  }
}

export default AuthCurrentService;
