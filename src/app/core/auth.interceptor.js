import appConfig from '../app.config.json';

function authInterceptor(AuthCurrentService, $window, $q){
  'ngInject';

  return {
    request(config) {
      if (config.url.startsWith(`appConfig.Api.baseUrl}/${appConfig.Api.version}/`) &&
        (!config.headers.Authorization)) {
        console.log("authentification information missing");
        return $q.reject(config);
      }

      return config;
    },

    responseError(response) {
      // 401 from server
      if (response.status === undefined) {
        AuthCurrentService.setCurrentToken('');
        $window.location.reload();
        return $q.reject(response);
      }

      // Rejected from request
      if (!response.headers.Authorization && response.method) {
        return $q.reject(response);
      }

      if (response.status === 403 && !response.success) {
        AuthCurrentService.setCurrentToken('');
        return $q.reject(response);
      }

      if (response.status === undefined || response.status === 401 || response.status === 403) {
        AuthCurrentService.setCurrentToken('');
        $window.location.reload();
      }

      return $q.reject(response);
    }
  };
}

export default authInterceptor;
