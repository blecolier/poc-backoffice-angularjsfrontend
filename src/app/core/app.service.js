import appConfig from '../app.config.json';

class AppService{
  constructor(AuthCurrentService){
    this.currentApplicationTitle = '';
    this.applicationEnvironment = appConfig.ApplicationEnvironment;
    this._authCurrent = AuthCurrentService;

    this.currentToken = this._authCurrent.getCurrentToken();
    this.currentUser = null;

    if(this.currentToken !== undefined && this.currentToken !== '')
      this.currentUser = this._authCurrent.extractUserFromToken();

    this.logedIn = this.currentToken !== undefined && this.currentToken !== '';
  }

  reloadAppTitle(appTitle){
    this.currentApplicationTitle = appTitle;
  }

  getAppEnvironment() {
    return this.applicationEnvironment;
  }

  getAppTitle(){
    return this.currentApplicationTitle;
  }

  userLogedIn(){
    this.currentUser = this._authCurrent.extractUserFromToken();
    this.logedIn = true;
  }

  getUserName(){
    return this.currentUser ? this.currentUser.displayName : '';
  }
}

export default AppService;
