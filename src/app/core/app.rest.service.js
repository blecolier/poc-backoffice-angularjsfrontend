import appConfig from '../app.config.json';

function addAuthHeader(url, token){
  if(url.startsWith(`/${appConfig.Api.version}/`) && token !== null)
    return {headers:{Authorization: token}};

  return {};
}


class RestAuthService{
  constructor(AuthService, $http){
    'ngInject'

    this._authService = AuthService;
    this._$http = $http;
  }

  post(url, data){
    return this._authService.refreshTokenIfNecessary()
      .then(() => {
        let config = addAuthHeader(url, this._authService.getAuthenticationHeader());
        return this._$http.post(appConfig.Api.baseUrl + url, data, config);
      }
    );
  }

  get(url){
    return this._authService.refreshTokenIfNecessary()
      .then(() => {
        let config = addAuthHeader(url, this._authService.getAuthenticationHeader());
        return this._$http.get(appConfig.Api.baseUrl + url, config);
      }
    );
  }

  put(url, data){
    return this._authService.refreshTokenIfNecessary()
      .then(() => {
        let config = addAuthHeader(url, this._authService.getAuthenticationHeader());
        return this._$http.put(appConfig.Api.baseUrl + url, data, config);
      }
    );
  }

  delete(url){
    return this._authService.refreshTokenIfNecessary()
      .then(() => {
        let config = addAuthHeader(url, this._authService.getAuthenticationHeader());
        return this._$http.delete(appConfig.Api.baseUrl + url, config);
      }
    );
  }
}

export default RestAuthService;
