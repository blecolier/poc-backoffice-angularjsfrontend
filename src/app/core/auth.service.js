import appConfig from '../app.config.json';

function needToRefreshToken(token, expirationDate){
  if(!token || expirationDate === 0)
    return false;

  // token expiration property is defined in seconds according to the RFC
  const date = Date.now() / 1000 + appConfig.Api.tokenExpirationLimit * 60;
  return expirationDate < date;
}

function hasTokenExpired(expirationDate){
  if(expirationDate === 0)
    return true;

  return expirationDate <= Date.now() / 1000;
}


class AuthService{
  constructor($http, $interval, AuthCurrentService, $q){
    'ngInject';

    this._$http = $http;
    this._$q = $q;
    this._authCurrent = AuthCurrentService;
  }

  getAuthenticationHeader(){
    return this._authCurrent.getAuthenticationHeader();
  }

  refreshTokenIfNecessary(){
    if(!needToRefreshToken(this._authCurrent.getCurrentToken(), this._authCurrent.getTokenExpirationDate())){
      return this._$q.when({});
    }

    return this._$http.post(`${appConfig.Api.baseUrl}/refresh`, { }, {headers:{Authorization: this.getAuthenticationHeader()}})
      .then((result) => {
          this._authCurrent.setCurrentToken(result.data.token);
          return this._$q.when(result);
        }
      );
  }

  logout(){
    this._$http.post(`${appConfig.Api.baseUrl}/logout`, { }, {headers:{Authorization: this.getAuthenticationHeader()}})
      .then(() => {
          this._authCurrent.destroyToken();
        }
    );
  }

  authenticate(credentials, callback){
    this._$http.post(`${appConfig.Api.baseUrl}/login`, { credentials : credentials })
    .then((result) => {
        this._authCurrent.setCurrentToken(result.data.token);
        callback(true);
      },
      () => { callback(null); }
    );
  }

  verifyAuth(){
    let deferred = this._$q.defer();
    let token = this._authCurrent.getCurrentToken();

    if(token !== null && token !== '' && !hasTokenExpired(this._authCurrent.getTokenExpirationDate())){
      deferred.resolve(true);
    }
    else {
      this._authCurrent.destroyToken();
      deferred.resolve(false);
    }

    return deferred.promise;
  }
}

export default AuthService;
