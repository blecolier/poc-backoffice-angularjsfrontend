/**
 * <userManagement> component
 *
 * @author <> <>
 */

 import controller from './user-management.controller';
 import './user-management.scss';

const userManagementComponent = {
  controller,
  templateUrl: './app/components/user-management/user-management.template.html',
};

export default userManagementComponent;
