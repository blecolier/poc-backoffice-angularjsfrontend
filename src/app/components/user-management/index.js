/**
 *  Shared  <userManagement> component
 *
 * @author <> <>
 */

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import userManagementComponent from './user-management.component';
import userListComponent from './components/userList.component';
import userDetailComponent from './components/userDetail.component';

var componentName = 'usermanagement';

const userManagement = angular
  .module('user.management', [uiRouter])
  .component(componentName, userManagementComponent)
  .component('userList', userListComponent)
  .component('userDetail', userDetailComponent)
  .config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
      .state(componentName, {
        url: '/usermanagement',
        template: '<usermanagement></usermanagement>',
        resolve: {
          auth : function(AuthService){
            return AuthService.verifyAuth(componentName);
          }
        }
      });
    $urlRouterProvider.otherwise('/');
  })
  .name;

export default userManagement;
