/**
 * userManagement component controller
 *
 * @author <> <>
 */

import BaseComponentController from '../../core/app.baseComponentController';

class UserManagementController extends BaseComponentController{

  constructor(AppService, RestAuthService) {
    'ngInject';

     super(AppService, RestAuthService, 'User Management');

     this.selectedUser = null;
     this.users = [];
     this.refreshUserList();
  }

  refreshUserList(){
    this.restAuthService.get('/v1/user')
      .then(res => {
          this.users = res.data.users;
        }
      )
      .catch(error => {
        this.error('Error while obtaining user list', error);
      });
  }

   getUsers(){
     return this.users;
   }

   selectUser(user){
     this.selectedUser = user;
   }

   isUserSelected(userId){
     return (this.selectedUser!== null && userId === this.selectedUser._id);
   }

   getSelectedUser(){
     return this.selectedUser;
   }

   getModules(user) {
    if (user !== null && typeof user !== 'undefined') {
      let modules = '';
      user.modules.forEach(function (item) {
          if (typeof item.moduleName !== 'undefined') {
              modules = `${modules}  ${item.moduleName} `;
          }
      });

      if (modules.length > 40) {
          modules = `${modules.substring(0, 40)}...`;
      }
      return modules;
    }
  }

   editUser(user){
     this.restAuthService.put(`/v1/user/${user._id}`, user)
     .then(res => {this.success('user successfully edited');})
     .catch(error => { this.error('Error while editing the user', error);});
   }

   deleteUser(userId){
     this.restAuthService.delete(`/v1/user/${userId}`)
       .then(res => {
         this.success('user successfully deleted');
         this.selectedUser = null;
         this.refreshUserList();
       })
       .catch(error => { this.error('Error while removing the user from the list', error);});
   }
}

export default UserManagementController;
