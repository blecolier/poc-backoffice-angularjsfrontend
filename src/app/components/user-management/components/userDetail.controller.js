/**
 * user detail controller
 *
 * @author <> <>
 */

import BaseComponentController from '../../../core/app.baseComponentController';

function getComponentName(module){
  module = module.module || module;
  return module.componentName;
}

class UserDetailController extends BaseComponentController{

  constructor(AppService, RestAuthService, $uibModal) {
    'ngInject';

    super(AppService, RestAuthService, 'User Management');

    this.permission = [
        {
            'key': 'read',
            'value':'read'
        },
        {
            'key': 'modify',
            'value': 'modify'
        },
        {
            'key': 'delete',
            'value': 'delete'
        },
        {
            'key': 'audit',
            'value': 'audit'
        }
    ];

    this._$uibModal = $uibModal;
    this.getModuleData();
  }

  getModuleData(){
    this.restAuthService.get('/v1/component/active')
      .then(res => {
        this.modules = res.data.components;
      })
      .catch(error => { this.error('Error while obtaining user list', error);});
  }

  hasUser(){
    return this.user !== null;
  }

  bindModulePermissions(module){
    let moduleName = typeof module.module === 'string' ? module.module : getComponentName(module);

    for (let i = 0; i < this.modules.length; i++) {
      if (this.modules[i].componentName.indexOf(moduleName) === -1) {
        continue;
      }

      for (let j = 0; j < this.user.modules.length; j++) {
        if (this.user.modules[j].moduleName.indexOf(moduleName) === -1) {
          continue;
        }

        this.modules[i].permissionEntity = {};
        this.modules[i].permissionEntity.read = this.user.modules[j].permission.filter(item => {return item === 'read';})[0] === 'read';
        this.modules[i].permissionEntity.modify = this.user.modules[j].permission.filter(item => {return item === 'modify';})[0] === 'modify';
        this.modules[i].permissionEntity.delete = this.user.modules[j].permission.filter(item => {return item === 'delete';})[0] === 'delete';

        if (this.modules[i].permissionEntity.read || this.modules[i].permissionEntity.modify || this.modules[i].permissionEntity.delete) {
          this.modules[i].selected = true;
        }
      }
    }
  }

  toggleModule(module){
    let moduleName = typeof module.module === 'string' ? module.module : getComponentName(module);

    for (let i = 0; i < this.user.modules.length; i++) {
      if (this.user.modules[i].moduleName.indexOf(moduleName) > -1) {
        this.user.modules.splice(i, 1);

        module = module.module || module;
        this.setPermission(module, 'read', false);
        this.setPermission(module, 'modify', false);
        this.setPermission(module, 'delete', false);
        module.selected = false;

        return;
      }
    }

    let newModule = {moduleName: module.module.componentName, permission : ['read'], permissionEntity : {}}
    this.user.modules.push(newModule);
    this.togglePermission(newModule, 'read')
    newModule.selected = true;


  }

  setPermission(module, permission, value) {
      switch (permission) {
          case 'read':
              module.permissionEntity.read = value;
              break;
          case 'modify':
              module.permissionEntity.modify = value;
              break;
          case 'delete':
              module.permissionEntity.delete = value;
              break;
          case 'audit':
              module.permissionEntity.audit = value;
              break;
          default:
      }
  }

  togglePermission(module, permission){
    if (!module.permissionEntity.read && (permission === 'modify' || permission === 'delete')) {
        module.permissionEntity = {};
        return;
    }

    for (let i = 0; i < this.user.modules.length; i++) {
      if (this.user.modules[i].moduleName.indexOf(module.componentName) === -1) {
        if(permission === 'read'){
          this.setPermission(module, permission, false);
          return;
        }

        continue;
      }

      for (let j = 0; j < this.user.modules[i].permission.length; j++) {
        if (this.user.modules[i].permission.length > 1 && permission === 'read') {
            return;
        }

        if (this.user.modules[i].permission[j] === permission) {
            if (permission === 'read') {
                this.toggleModule(module);
                return;
            }

            this.user.modules[i].permission.splice(j, 1);
            this.setPermission(module, permission, false);
            return;
        }
      }

      this.user.modules[i].permission.push(permission);
      this.setPermission(module, permission, true);
      return;
    }
  }
}

export default UserDetailController;
