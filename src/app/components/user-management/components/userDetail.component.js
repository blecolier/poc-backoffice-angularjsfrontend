import userDetailController from './userDetail.controller'

const userDetailComponent = {
  controller: userDetailController,
  templateUrl: './app/components/user-management/components/userDetail.template.html',
  bindings:{
    user: '<',
    onEdited: '&',
    onDeleted: '&'
  }
};

export default userDetailComponent;
