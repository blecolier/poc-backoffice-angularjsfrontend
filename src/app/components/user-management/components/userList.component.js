import userListController from './userList.controller'

const userListComponent = {
  controller: userListController,
  templateUrl: './app/components/user-management/components/userList.template.html',
  bindings:{
    users: '=',
    onSelected: '&',
    isSelected: '&'
  }
};

export default userListComponent;
