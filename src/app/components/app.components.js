import angular from 'angular';
import test1 from './test1';
import test2 from './test2';
import dashboard from './dashboard';
import authManagement from './auth-management';
import userManagement from './user-management';

const components = angular
  .module('app.components', [
    test1,
    test2,
    dashboard,
    authManagement,
    userManagement,
  ]).name;

export default components;
