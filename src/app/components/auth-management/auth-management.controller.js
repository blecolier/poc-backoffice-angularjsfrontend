/**
 * authManagement component controller
 *
 * @author <> <>
 */
import BaseComponentController from '../../core/app.baseComponentController';

class AuthManagementController extends BaseComponentController{

  constructor(AppService, RestAuthService) {
    'ngInject';

     super(AppService, RestAuthService, 'Component Management');

     this.selectedComponent = null;
     this.components = [];
     this.refreshComponentList();
  }

  refreshComponentList(){
    this.restAuthService.get('/v1/component')
      .then(res => {
          this.components = res.data.components;
        }
      )
      .catch(error => { this.error('Error while obtaining component list', error);});
  }

  getComponents(){
    return this.components;
  }

  getSelectedComponent(){
    return this.selectedComponent;
  }

  selectComponent(component){
    this.selectedComponent = component;
  }

  clearComponent(){
    this.selectedComponent = null;
  }

  isComponentSelected(componentId){
    return (this.selectedComponent !== null && componentId === this.selectedComponent._id);
  }

  deleteComponent(componentId){
    this.restAuthService.delete(`/v1/component/${componentId}`)
      .then(res => {
        this.success('component successfully deleted');
        this.selectedComponent = null;
        this.refreshComponentList();
      })
      .catch(error => { this.error('Error while removing component from the list', error);});
  }

  editComponent(component){
    this.restAuthService.put(`/v1/component/${component._id}`, component)
      .then(res => {this.success('component successfully edited');})
      .catch(error => { this.error('Error while editing the component', error);});
  }

  saveComponent(component){
    this.restAuthService.post('/v1/component', component)
      .then(res => {
        this.success('component successfully saved');
        this.refreshComponentList();
      })
      .catch(error => { this.error('Error while saving the component', error);});
  }
}

export default AuthManagementController;
