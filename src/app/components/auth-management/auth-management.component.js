/**
 * <authManagement> component
 *
 * @author <> <>
 */

import controller from './auth-management.controller';
import './auth-management.scss';

const authManagementComponent = {
  controller,
  templateUrl: './app/components/auth-management/auth-management.template.html',
};

export default authManagementComponent;
