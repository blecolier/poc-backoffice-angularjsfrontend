/**
 *  Shared  <authManagement> component
 *
 * @author <> <>
 */

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import authManagementComponent from './auth-management.component';
import componentListComponent from './components/componentList.component';
import componentDetailComponent from './components/componentDetail.component';

var componentName = 'authmanagement';

const authManagement = angular
  .module('auth.management', [uiRouter])
  .component(componentName, authManagementComponent)
  .component('componentList', componentListComponent)
  .component('componentDetail', componentDetailComponent)
  .config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
      .state(componentName, {
        url: '/authmanagement',
        template: '<authmanagement></authmanagement>',
        resolve: {
          auth : AuthService => {
            return AuthService.verifyAuth(componentName);
          }
        }
      });
    $urlRouterProvider.otherwise('/');
  })
  .name;

export default authManagement;
