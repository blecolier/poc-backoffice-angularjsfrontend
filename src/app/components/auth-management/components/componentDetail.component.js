import componentDetailController from './componentDetail.controller'

const componentDetailComponent = {
  controller: componentDetailController,
  templateUrl: './app/components/auth-management/components/componentDetail.template.html',
  bindings:{
    component: '<',
    onSaved: '&',
    onEdited: '&',
    onCleared: '&'
  }
};

export default componentDetailComponent;
