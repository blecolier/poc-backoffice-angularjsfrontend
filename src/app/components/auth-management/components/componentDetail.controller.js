/**
 * component detail controller
 *
 * @author <> <>
 */

class ComponentDetailController {

  isNewComponent(){
    return this.component === null || this.component._id === null || typeof this.component._id === 'undefined';
  }
}

export default ComponentDetailController;
