import componentListController from './componentList.controller'

const componentListComponent = {
  controller: componentListController,
  templateUrl: './app/components/auth-management/components/componentList.template.html',
  bindings:{
    components: '=',
    onSelected: '&',
    onDeleted: '&',
    isSelected: '&'
  }
};

export default componentListComponent;
