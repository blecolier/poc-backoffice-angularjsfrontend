/**
 *  <test1> component
 *
 * @author <> <>
 */

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import test1Component from './test1.component';

const test1 = angular
  .module('test1', [uiRouter])
  .component('test1', test1Component)
  .config(($stateProvider, $urlRouterProvider, $httpProvider) => {
    $stateProvider
      .state('test1', {
        url: '/test1',
        template: '<test1></test1>',
        resolve: {
          auth : function(AuthService){
            return AuthService.verifyAuth();
          }
        }
      });
    $urlRouterProvider.otherwise('/');
  })
  .name;

export default test1;
