/**
 * <test1> component
 *
 * @author <> <>
 */

import Test1Controller from './test1.controller';
import './test1.scss';

const test1Component = {
  controller: Test1Controller,
  templateUrl: './app/components/test1/test1.template.html'
};

export default test1Component;
