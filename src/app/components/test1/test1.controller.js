/**
 * test1 component controller
 *
 * @author <> <>
 */

import BaseComponentController from '../../core/app.baseComponentController';

class Test1Controller extends BaseComponentController{

  constructor(AppService, RestAuthService) {
    'ngInject';

    super(AppService, RestAuthService, 'test page 1!!');
  }
}

export default Test1Controller;
