/**
 * <test2> component
 *
 * @author <> <>
 */

import Test2Controller from './test2.controller';
import './test2.less';

const test2Component = {
  controller: Test2Controller,
  templateUrl: './app/components/test2/test2.template.html',
  bindings: {
    name: '@',
    onLoaded: '&'
  }
};

export default test2Component;
