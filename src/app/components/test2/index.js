/**
 *  <test2> component
 *
 * @author <> <>
 */

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import test2Component from './test2.component';

const test2 = angular
  .module('test2', [uiRouter])
  .component('test2', test2Component)
  .config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
      .state('test2', {
        url: '/test2',
        template: '<test2></test2>',
        resolve: {
          auth : function(AuthService){
            return AuthService.verifyAuth();
          }
        }
      });
    $urlRouterProvider.otherwise('/');
  })
  .name;

export default test2;
