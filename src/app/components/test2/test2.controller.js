/**
 * test2 component controller
 *
 * @author <> <>
 */

import BaseComponentController from '../../core/app.baseComponentController';

class Test2Controller extends BaseComponentController{

  constructor(AppService, RestAuthService) {
    'ngInject';

    var title = 'test page 2 or somthing!!';
    super(AppService, RestAuthService, title);
    this.pageName = title;
  }
}

export default Test2Controller;
