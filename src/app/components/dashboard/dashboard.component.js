/**
 * <dashboard> component
 *
 * @author <> <>
 */

import controller from './dashboard.controller';
import './dashboard.scss';

const dashboardComponent = {
  controller,
  templateUrl: './app/components/dashboard/dashboard.template.html',
};

export default dashboardComponent;
