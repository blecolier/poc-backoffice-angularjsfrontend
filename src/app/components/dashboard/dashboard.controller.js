/**
 * dashboard component controller
 *
 * @author <> <>
 */
import BaseComponentController from '../../core/app.baseComponentController';

class DashboardController  extends BaseComponentController{

  constructor(AppService, RestAuthService) {
    'ngInject';

     super(AppService, RestAuthService, 'Dashboard');
  }

}

export default DashboardController;
