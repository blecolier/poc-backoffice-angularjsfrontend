/**
 *  <dashboard> component
 *
 * @author <> <>
 */

import angular from 'angular';
import uiRouter from 'angular-ui-router';
import dashboardComponent from './dashboard.component';

var componentName = 'dashboard';

const dashboard = angular
  .module(componentName, [uiRouter])
  .component(componentName, dashboardComponent)
  .config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
      .state(componentName, {
        url: '/dashboard',
        template: '<dashboard></dashboard>',
        resolve: {
          auth : function(AuthService){
            return AuthService.verifyAuth(componentName);
          }
        }
      });
    $urlRouterProvider.otherwise('/');
  })
  .name;

export default dashboard;
