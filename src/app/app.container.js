import appController from './app.controller';

const appContainer = {
  templateUrl: './app/app.container.html',
  controller: appController
};

export default appContainer;
