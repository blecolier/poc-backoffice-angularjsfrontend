/**
 *  <error> component
 *
 * @author <> <>
 */

import angular from 'angular';

import communicationComponent from './communication.component';

const communication = angular
  .module('communication', [])
  .component('communication', communicationComponent)
  .name;

export default communication;
