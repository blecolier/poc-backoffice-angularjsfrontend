/**
 * error component controller
 *
 * @author <> <>
 */

class CommunicationController {

  deleteMessage(index){
    let tmp = this.messages;
    tmp.splice(index,1);

    this.messages = [];

    for (let i = 0; i < tmp.length; i++) {
      this.messages.push(tmp[i]);
      this.messages[i].index = i;
    }
  }

  getMessageClass(messageType){
    switch(messageType){
      case 'success':return 'success';
      case 'warning':return 'warning';
      case 'error':
      default: return 'danger';
    }
  }

}

export default CommunicationController;
