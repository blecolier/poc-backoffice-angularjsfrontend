/**
 * <error> component
 *
 * @author <> <>
 */

import CommunicationController from './communication.controller';
import './communication.scss';

const communicationComponent = {
  controller : CommunicationController,
  templateUrl: './app/common/communication/communication.template.html',
  bindings: { messages: '=' }
};

export default communicationComponent;
