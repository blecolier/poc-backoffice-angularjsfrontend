/* eslint-disable no-unused-vars */

import angular from 'angular';
import navigation from './navigation';
import communication from './communication';
import landing from './landing';
import header from './header';
import footer from './footer';
import authentication from './authentication';

/* eslint-enable no-unused-vars */

const commons = angular
  .module('app.commons', ['navigation', 'header', 'footer', 'authentication', 'communication', 'landing']).name;

export default commons;
