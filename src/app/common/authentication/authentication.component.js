/**
 * <authentication> component
 *
 * @author <> <>
 */

import AuthenticationController from './authentication.controller';
import './authentication.scss';

const authenticationComponent = {
  controller : AuthenticationController,
  templateUrl: './app/common/authentication/authentication.template.html'
};

export default authenticationComponent;
