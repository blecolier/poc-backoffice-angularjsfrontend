/**
 * authentication component controller
 *
 * @author <> <>
 */

class AuthenticationController {

  constructor(AuthService, AppService, $location) {
    'ngInject';

    this.authService = AuthService;
    this.appService = AppService;
    this.loginError = false;
    this.UserName = this.appService.getUserName();
    this.logedIn = this.appService.logedIn;
    this._$location = $location;
    this.loginErrorMessage = ''
  }

  logout(){
    this.authService.logout();
  }

  submitForm(){
    if(this.formData === undefined || !this.formData.username || !this.formData.password){
      this.loginErrorMessage = 'Missing user or password';
      this.loginError = true;
      return;
    }

    this.authService.authenticate(this.formData, res => {
      if (res) {
        this.appService.userLogedIn();
        this.logedIn = this.appService.logedIn;
        this.loginError = false;
        this.UserName = this.appService.getUserName();
        this._$location.path('/dashboard')
      }
      else {
        this.loginErrorMessage = 'Wrong user or password';
        this.loginError = true;
      }
    }
  );
  }

}

export default AuthenticationController;
