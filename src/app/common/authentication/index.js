/**
 *  <authentication> component
 *
 * @author <> <>
 */

import angular from 'angular';

import authenticationComponent from './authentication.component';

const authentication = angular
  .module('authentication', [])
  .component('authentication', authenticationComponent)
  .name;

export default authentication;
