/**
 * <landing> component
 *
 * @author <> <>
 */

import landingController from './landing.controller';
import './landing.scss';

const landingComponent = {
  controller : landingController,
  templateUrl: './app/common/landing/landing.template.html',
};

export default landingComponent;
