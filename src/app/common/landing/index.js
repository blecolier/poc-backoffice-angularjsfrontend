/**
 *  <landing> component
 *
 * @author <> <>
 */

import angular from 'angular';

import landingComponent from './landing.component';

const landing = angular
  .module('landing', [])
  .component('landing', landingComponent)
  .name;

export default landing;
