/**
 * <footer> component
 *
 * @author <> <>
 */

import FooterController from './footer.controller';
import './footer.scss';

const footerComponent = {
  controller : FooterController,
  templateUrl: './app/common/footer/footer.template.html'
};

export default footerComponent;
