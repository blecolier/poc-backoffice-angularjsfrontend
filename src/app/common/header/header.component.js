/**
 * <header> component
 *
 * @author <> <>
 */

import HeaderController from './header.controller';
import './header.less';

const headerComponent = {
  controller: HeaderController,
  templateUrl: './app/common/header/header.template.html',
  bindings: { title: '@' }
};

export default headerComponent;
