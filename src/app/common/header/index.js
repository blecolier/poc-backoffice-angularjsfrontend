import angular from 'angular';
import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import headerComponent from './header.component';

const header = angular
  .module('header', [uiRouter, uiBootstrap])
  .component('header', headerComponent)
  .name;

export default header;
