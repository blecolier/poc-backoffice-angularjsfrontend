/**
 * <navigation> component
 *
 * @author <> <>
 */

import NavigationController from './navigation.controller';
import './navigation.scss';

const navigationComponent = {
  controller: NavigationController,
  templateUrl: './app/common/navigation/navigation.template.html'
};

export default navigationComponent;
