/**
 *  <navigation> component
 *
 * @author <> <>
 */

import angular from 'angular';

import navigationComponent from './navigation.component';

const navigation = angular
  .module('navigation', [])
  .component('navigation', navigationComponent)
  .name;

export default navigation;
