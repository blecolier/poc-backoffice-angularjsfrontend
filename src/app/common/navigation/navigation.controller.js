/**
 * navigation component controller
 *
 * @author <> <>
 */

import BaseComponentController from '../../core/app.baseComponentController';

class NavigationController extends BaseComponentController{

  constructor(AppService, RestAuthService) {
    'ngInject';

     super(AppService, RestAuthService, 'Component Management');

     this.userModules = AppService.currentUser.modules;
     this.modules = [];
     this.getModuleData();
   }


  getModuleData(){
    this.restAuthService.get('/v1/component/active')
      .then(res => {
        for (let j = 0; j < this.userModules.length; j++) {
          for (let i = 0; i < res.data.components.length; i++) {
            if (this.userModules[j].moduleName.indexOf(res.data.components[i].componentName) > -1) {
              this.modules.push(res.data.components[i]);
            }
          }
        }
      })
      .catch(error => { this.error('Error while obtaining user list', error);});
  }

}



export default NavigationController;
